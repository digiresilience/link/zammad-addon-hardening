class HardeningHardenSettings < ActiveRecord::Migration[5.2]
  def self.restore_setting(name)
    s = Setting.find_by(name: name)
    if !s.nil?
      s.state_current = s.state_initial
      s.save!
    end
  end
  def self.set_setting(name, value)
    s = Setting.find_by(name: name)
    if !s.nil?
      s.state_current = { "value" => value }
      s.save!
    end
  end
  def self.up
    ["ui_send_client_stats", "geo_ip_backend", "geo_location_backend", "image_backend", "geo_calendar_backend"].each { |n|
      self.set_setting(n, "")
    }

    # disable customer ticket creation
    self.set_setting("customer_ticket_create", false)

    # disable user account registration
    self.set_setting("user_create_account", false)

    # bump up min password length
    self.set_setting("password_min_size", 10)

    # delete default zammad user
    nicole = User.find_by(email: "nicole.braun@zammad.org")
    if !nicole.nil?
      Ticket.where(customer: nicole).destroy_all
      nicole.destroy
    end
  end

  def self.down
    ["ui_send_client_stats", "geo_ip_backend", "geo_location_backend", "image_backend", "geo_calendar_backend"].each { |n|
      self.restore_setting(n)
    }
    ["customer_ticket_create", "user_create_account", "password_min_size"].each { |n|
      self.restore_setting(n)
    }
  end
end
