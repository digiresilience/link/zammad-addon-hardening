# zammad-addon-hardening

A [Zammad](https://zammad.org) addon that hardens a Zammad instance according to CDR's needs.

The hardening applied consists of:

* Prevent sensitive information from being leaked in notification emails
* Zammad external services for geoip, maps, image, and calendar are disabled
* Disable client side stats
* Set minimum password length to 10
* Prevent customers from creating tickets in the web ui
* Disable user registration
* Remove default "nicole.braun@zammad.org" account

## Development

1. Edit the files in `src/`

   Migration files should go in `src/db/addon/hardening` ([see this post](https://community.zammad.org/t/automating-creation-of-custom-object-attributes/3831/2?u=abelxluck))

2. Update version and changelog in `hardening-skeleton.szpm`
3. Build a new package `make`

   This outputs `dist/hardening-vXXX.szpm`

4. Install the szpm using the zammad package manager.

5. Repeat


### Create a new migration

Included is a helper script to create new migrations. You must have the python
`inflection` library installed.

* debian/ubuntu:  `apt install python3-inflection`
* pip: `pip install --user inflection`
* or create your own venv

To make a new migration simply run:
```
make new-migration
```

## License

[![License GNU AGPL v3.0](https://img.shields.io/badge/License-AGPL%203.0-lightgrey.svg)](https://gitlab.com/digiresilience/link/zamamd-addon-hardening/blob/master/LICENSE.md)

This is a free software project licensed under the GNU Affero General
Public License v3.0 (GNU AGPLv3) by [The Center for Digital
Resilience](https://digiresilience.org) and [Guardian
Project](https://guardianproject.info).


🤠
